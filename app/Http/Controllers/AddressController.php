<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search');
    }
    public function search(Request $request)
    {
        $query = Address::getSearchQuery($request->query('q'));
        $addresses = $query->get();

        return response()->json($addresses);
    }

}
