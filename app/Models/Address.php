<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;
    public static $searchable = [
        'name',
        'street_name',
    ];
    public static function getSearchQuery($search){
        $query = Address::whereRaw("MATCH(".implode(',',Address::$searchable).") AGAINST(? IN BOOLEAN MODE)", array($search));
        $query->orWhere("street_number", 'like', '%' . $search . '%');
        return $query;
    }
}
