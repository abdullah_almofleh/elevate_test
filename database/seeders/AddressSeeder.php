<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $addresses = \App\Models\Address::factory(100)->make();

        $chunks = $addresses->chunk(1000);

        $chunks->each(function($chunk){
            \App\Models\Address::insert($chunk->toArray());
        });

    }
}
